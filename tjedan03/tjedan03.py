from PIL import Image
import os
from skimage import io
import numpy as np
import matplotlib.pyplot as plt
import skimage
from skimage import data

from scipy import ndimage
from scipy.ndimage import distance_transform_edt
from skimage import morphology, segmentation
from skimage.morphology import disk


"""IMPORTANT"""
from skimage import filters
from skimage import color
from skimage import feature
from skimage import measure




#%%
"""
# =============================================================================
# Image show gray
# =============================================================================
"""

def showGray(image):
    plt.imshow(image, cmap = 'gray')
    plt.show()
    
"""
# =============================================================================
# Function to compare two images 
# =============================================================================
"""
def compareImage(first, second, first_title, second_title):
    fig, axes = plt.subplots(ncols=2, figsize=(8, 5) )
    ax = axes.ravel()

    ax[0].imshow(first, cmap='gray')
    ax[0].set_title(first_title)
    #ax[0].axis('off')
    ax[1].imshow(second, cmap='gray')
    ax[1].set_title(second_title)
    #ax[1].axis('off')
    plt.show()
"""
# =============================================================================
# Convert image to grayscale
# =============================================================================
"""

def convert2Gray(image):
    image = color.rgb2gray(image)
    return image

#%%
"""
# =============================================================================
#   Gaussian filter
# =============================================================================
"""

astronaut = data.astronaut()
# Appying gaussian_filter
# check sigma
sigma = 3
gaussian_filter = filters.gaussian(astronaut, sigma, multichannel=True)

compareImage(astronaut, gaussian_filter, "Orginal", "Gaussian filter with sigma {}".format(sigma))

#%%
"""
# =============================================================================
#   Median filter
# =============================================================================
"""

disk_number = 5

camera = data.camera()
median_filter = filters.median(camera, disk(disk_number))
compareImage(camera, median_filter, "Orginal", "Median filter disk= {}".format(disk_number))


#%%
from skimage import exposure

my_image = exposure.equalize_adapthist(astronaut, clip_limit = 0.03)

compareImage(astronaut, my_image, "1", "2")


#%%
"""
# =============================================================================
# FFT
# =============================================================================
"""
def fourierTransform(image, mask):
    #Fourier Transform
    image_ft = np.fft.fft2(image)
    #Shift center from top left conter to center
    image_ft_shift = np.fft.fftshift(image_ft)
    #Mask
    image_ft_shift_m = image_ft_shift*mask
    #Inverse image
    image_inverse = np.fft.ifft2(image_ft_shift_m)
    return np.abs(image_inverse)

def detailFourierTransform(image, mask):
    #Fourier Transform
    image_ft = np.fft.fft2(image)
    #Show fft
    #To see picture use .astype(np.uint8) to avoid complex numbers
    compareImage(image, (np.abs(image_ft).astype(np.uint8)), "Input Image", "FT Image")
    #Shift
    image_ft_shift = np.fft.fftshift(image_ft)
    #Mask
    image_ft_shift_m = image_ft_shift*mask
    compareImage(np.abs(image_ft_shift).astype(np.uint8), np.abs(image_ft_shift_m).astype(np.uint8), "Shifted FT Image", "Masked Shifted FT Image")
    #Inverse image
    image_inverse = np.fft.ifft2(image_ft_shift_m)
    compareImage(image, np.abs(image_inverse), "Input Image", "IFT output Image")

#%%

"""
# =============================================================================
# MASKS (Filters)
# =============================================================================
"""
# High Pass Filter (HPF)
# Circular HPF mask, center circle is 0, remaining all ones
def maskHPF(image, radius):
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.ones((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
    mask[mask_area] = 0
    return mask    

# Low Pass Filter (LPF) (Haming window filter)
# LPF Filter - Circular LPF mask, center circle is 1, remaining all zeros
def maskLPF(image, radius): 
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
    mask[mask_area] = 1
    return mask


# Band Pass Filter
def maskBPF(image, out_radius,in_radius):
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r_out = out_radius
    r_in = in_radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = np.logical_and(((x - center[0]) ** 2 + (y - center[1]) ** 2 >= r_in ** 2),((x - center[0]) ** 2 + (y - center[1]) ** 2 <= r_out ** 2))
    mask[mask_area] = 1
    return mask

#Gaussian filter.
def maskGaussian(image, x, y):
    sigmax, sigmay = x, y
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)
    x = np.linspace(0, rows, rows)
    y = np.linspace(0, cols, cols)
    X,Y = np.meshgrid(x, y)
    mask = np.exp(-(((X-ccol)/sigmax)**2 + ((Y-crow)/sigmay)**2))
    return mask

#%%
"""
# =============================================================================
# TEST FT with masks(filters)
# =============================================================================
"""
image_1 = data.astronaut()
image_1 = convert2Gray(image_1)

image_2 = data.camera()

image_3 = data.binary_blobs()
image_3 = convert2Gray(image_3)

print("HPF mask")
#showGray(fourierTransform(image_1, maskLPF(image_1,15)))
detailFourierTransform(image_1,maskHPF(image_1,15))
#print("\n\n\n")

print("LPF mask")
#showGray(fourierTransform(image_1, maskLPF(image_1,30)))
detailFourierTransform(image_2,maskLPF(image_1,25))
#print("\n\n\n")

print("BPF mask")
#showGray(fourierTransform(image_1, maskBPF(image_1,140,40)))
detailFourierTransform(image_3,maskBPF(image_1,160,70))
#print("\n\n\n")

print("Gaussian mask")
detailFourierTransform(image_1,maskGaussian(image_1, 35,35))










































