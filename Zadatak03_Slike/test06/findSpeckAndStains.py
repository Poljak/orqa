import os
from skimage import io
import numpy as np
import matplotlib.pyplot as plt
from skimage import filters
from skimage import color
from skimage import exposure
import shutil
import cv2
from scipy import ndimage
from skimage import morphology
from skimage import measure
#%%
"""
# =============================================================================
#   METHODS
# =============================================================================
"""
def ShowGray(name):
    plt.figure(figsize=(20,20))
    plt.imshow(name, cmap='gray')
    
def fourierTransform(image, mask):
    #Fourier Transform
    image_ft = np.fft.fft2(image)
    #Shift center from top left conter to center
    image_ft_shift = np.fft.fftshift(image_ft)
    #Mask
    image_ft_shift_m = image_ft_shift*mask
    #Inverse image
    image_inverse = np.fft.ifft2(image_ft_shift_m)
    return np.abs(image_inverse)

def Gaussian(image, x, y):
    sigmax, sigmay = x, y
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)
    x = np.linspace(0,rows, rows)
    y = np.linspace(0,cols, cols)
    X,Y = np.meshgrid(x[0], y[0])
    mask = np.exp(-(((X-ccol)/sigmax)**2 + ((Y-crow)/sigmay)**2))
    return mask

def BPF(image, out_radius,in_radius):
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r_out = out_radius
    r_in = in_radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = np.logical_and(((x - center[0]) ** 2 + (y - center[1]) ** 2 >= r_in ** 2),((x - center[0]) ** 2 + (y - center[1]) ** 2 <= r_out ** 2))
    mask[mask_area] = 255
    return mask

def HPF(image, radius):
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.ones((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
    mask[mask_area] = 200
    return mask

def LPF(image, radius): 
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r 
 
    mask[mask_area] = 1
    return mask

def FindSpecks(image_gray):
    gaussian_filter = ndimage.gaussian_filter(image_gray, 3)
    gamma_corrected = exposure.adjust_gamma(gaussian_filter , 5) 
    sobel = filters.sobel(gamma_corrected)    
    sobel = exposure.adjust_gamma(sobel, 0.80)
    maximum_filter = ndimage.maximum_filter(sobel ,  size=5,  mode='nearest')
    gamma_corrected2 = exposure.adjust_gamma(maximum_filter, 0.95)
    threshold = filters.threshold_yen(gamma_corrected2)
    small_objects = gamma_corrected2>threshold
    remove_small_objects = morphology.remove_small_objects(small_objects, min_size=220)
    return remove_small_objects

def FindStains(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # do adaptive threshold on gray image
    thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 581, 9)
    # apply morphology open then close
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4))
    blob = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (13,13))
    blob = cv2.morphologyEx(blob, cv2.MORPH_CLOSE, kernel)

    blob = (255 - blob)
    
    #find all your connected components (white blobs in your image)
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(blob, connectivity=8)
    sizes = stats[1:, -1]; nb_components = nb_components - 1
    
    # minimum size of particles we want to keep (number of pixels)
    min_size = 1700
    solution = np.zeros((output.shape))
    
    for i in range(0, nb_components):
        if sizes[i] >= min_size:
            solution[output == i + 1] = 255
    #resize
    solution = solution[1000:2800, 1100:3700]
    return solution

def Counter(image):
    image_label = measure.label(image, background=0)
    propsa = measure.regionprops(image_label)
    length = len(propsa)
    return length-1

def WriteReport(image_1, image_2):
    status= "Clean"
    result = '  Image name: {}\n  Status: {}\n'.format(image_name,status)
    if(Counter(image_1) > 0):
        status = "Dirty"
        result = '  Image name: {}\n  Status: {}\n'.format(image_name,status)
        return result
    if(Counter(image_2) > 0):
        status = "Dirty"
        result = '  Image name: {}\n  Status: {}\n'.format(image_name,status)
        return result
    return result

def CreateFolder(dirName):
    if not os.path.exists(dirName):
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ")
    else:
        shutil.rmtree(dirName)
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ")

def ImageReportSpecks(image_result):
    path = os.path.abspath(os.getcwd())
    plt.imsave("{}/{}/SpecksTested_{}".format(path,dirName,image_name), image_result, cmap = plt.cm.gray )

def ImageReportStains(image_result):
    path = os.path.abspath(os.getcwd())
    plt.imsave("{}/{}/StainsTested_{}".format(path,dirName,image_name), image_result, cmap = plt.cm.gray ) 

#%%

"""
# =============================================================================
# Clean Test
# =============================================================================
"""

#Path to your images for testing
images_path = "C:/Users/Deni/Desktop/workOrqa/zadatak3/Specks test/Clean/"
dirName = "Clean_test"


CreateFolder(dirName)
file_report = open(os.path.join(os.path.abspath(os.getcwd()),"Report_Clean.txt"),"w+")
for index, file in enumerate(os.listdir(images_path)):
    if file.endswith('.png'):
        image_name = file
        image = cv2.imread(images_path + image_name)
        image_gray = color.rgb2gray(image)
        image_result_1 = FindSpecks(image_gray)
        image_result_2 = FindStains(image)
        ImageReportSpecks(image_result_1)
        ImageReportStains(image_result_2)
        report = WriteReport(image_result_1, image_result_2)
        file_report.write("Image: {}\n".format(index+1))
        file_report.write(report)
        print("{} finished".format(image_name))
file_report.close()
#%%
    
"""
# =============================================================================
# Dirty Test
# =============================================================================
"""
#Path to your images for testing
images_path = "C:/Users/Deni/Desktop/workOrqa/zadatak3/Specks test/Dirty/"
dirName = "Dirty_test"


CreateFolder(dirName)
file_report = open(os.path.join(os.path.abspath(os.getcwd()),"Report_Dirty.txt"),"w+")
for index, file in enumerate(os.listdir(images_path)):
    if file.endswith('.png'):
        image_name = file
        image = cv2.imread(images_path + image_name)
        image_gray = color.rgb2gray(image)
        image_result_1 = FindSpecks(image_gray)
        image_result_2 = FindStains(image)
        ImageReportSpecks(image_result_1)
        #ImageReportStains(image_result_2)
        report = WriteReport(image_result_1, image_result_2)
        file_report.write("Image: {}\n".format(index+1))
        file_report.write(report)
        print("{} finished".format(image_name))
file_report.close()













