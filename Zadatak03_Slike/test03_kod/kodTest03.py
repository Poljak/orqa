import os
from skimage import io
import numpy as np
import matplotlib.pyplot as plt
from skimage import filters
from skimage import color
from skimage import exposure
#%%

#%%
"""
# =============================================================================
#   METHODS
# =============================================================================
"""

def Show(name):
    plt.figure(figsize=(15,15))
    plt.imshow(name)

def ShowGray(name):
    plt.figure(figsize=(20,20))
    plt.imshow(name, cmap='gray')
    
def Convert2Gray(image):
    image = color.rgb2gray(image)
    return image

def fourierTransform(image, mask):
    #Fourier Transform
    image_ft = np.fft.fft2(image)
    #Shift center from top left conter to center
    image_ft_shift = np.fft.fftshift(image_ft)
    #Mask
    image_ft_shift_m = image_ft_shift*mask
    #Inverse image
    image_inverse = np.fft.ifft2(image_ft_shift_m)
    return np.abs(image_inverse)

def maskLPF(image, radius): 
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
 
    mask[mask_area] = 1
    return mask

#%%

listOfDirtyImage = []
 
for file in os.listdir("C:/Users/Deni/Desktop/workOrqa/zadatak3/Specks test/Dirty"):
    if file.endswith('.png'):
        #print(file)
        listOfDirtyImage.append(file)
        

        
#%%
path = "C:/Users/Deni/Desktop/workOrqa/zadatak3/Specks test/Dirty/"


image_name = listOfDirtyImage [1] #Change the image manually
image = io.imread(path + image_name)
image_gray = color.rgb2gray(image)

#%%
"""
# =============================================================================
#   TEST
# =============================================================================
"""    

#Reduce ringing
p1 = fourierTransform(image_gray,maskLPF(image_gray, 450))


p2 = filters.gaussian(p1, sigma = 2)


p3 = exposure.adjust_gamma(p2, 7)

p4 = filters.threshold_niblack(p3, k=1.3)

ShowGray(p4)

    












