import os
from skimage import io
import numpy as np
import matplotlib.pyplot as plt
from skimage import filters
from skimage import color
from skimage import exposure
import shutil

from scipy import ndimage
from skimage import morphology
from skimage import measure
#%%
"""
# =============================================================================
#   METHODS
# =============================================================================
"""
def ShowGray(name):
    plt.figure(figsize=(20,20))
    plt.imshow(name, cmap='gray')
    
def fourierTransform(image, mask):
    #Fourier Transform
    image_ft = np.fft.fft2(image)
    #Shift center from top left conter to center
    image_ft_shift = np.fft.fftshift(image_ft)
    #Mask
    image_ft_shift_m = image_ft_shift*mask
    #Inverse image
    image_inverse = np.fft.ifft2(image_ft_shift_m)
    return np.abs(image_inverse)

def Gaussian(image, x, y):
    sigmax, sigmay = x, y
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)
    x = np.linspace(0,rows, rows)
    y = np.linspace(0,cols, cols)
    X,Y = np.meshgrid(x[0], y[0])
    mask = np.exp(-(((X-ccol)/sigmax)**2 + ((Y-crow)/sigmay)**2))
    return mask

def BPF(image, out_radius,in_radius):
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r_out = out_radius
    r_in = in_radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = np.logical_and(((x - center[0]) ** 2 + (y - center[1]) ** 2 >= r_in ** 2),((x - center[0]) ** 2 + (y - center[1]) ** 2 <= r_out ** 2))
    mask[mask_area] = 255
    return mask

def HPF(image, radius):
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.ones((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
    mask[mask_area] = 200
    return mask

def LPF(image, radius): 
    rows, cols = image.shape
    crow, ccol = int(rows / 2), int(cols / 2)

    mask = np.zeros((rows, cols), np.uint8)
    r = radius
    center = [crow, ccol]
    x, y = np.ogrid[:rows, :cols]
    mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r 
 
    mask[mask_area] = 1
    return mask

def FindSpecks(image_gray):
    gaussian_filter = ndimage.gaussian_filter(image_gray, 3)
    gamma_corrected = exposure.adjust_gamma(gaussian_filter , 5) 
    sobel = filters.sobel(gamma_corrected)    
    sobel = exposure.adjust_gamma(sobel, 0.80)
    maximum_filter = ndimage.maximum_filter(sobel ,  size=5,  mode='nearest')
    gamma_corrected2 = exposure.adjust_gamma(maximum_filter, 0.95)
    threshold = filters.threshold_yen(gamma_corrected2)
    small_objects = gamma_corrected2>threshold
    remove_small_objects = morphology.remove_small_objects(small_objects, min_size=207)
    return remove_small_objects

def WriteReport(image):
    image_label = measure.label(image, background=0)
    propsa = measure.regionprops(image_label)
    length = len(propsa)
    status= "Clean"
    if(length-1 > 0):
        status = "Dirty"
    result = '  Image name: {}\n  Specks = {}\n  Status: {}\n'.format(image_name, length-1, status)
    return result

def CreateFolder(dirName):
    if not os.path.exists(dirName):
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ")
    else:
        shutil.rmtree(dirName)
        os.mkdir(dirName)
        print("Directory " , dirName ,  " Created ")

def ImageReport():
    path = os.path.abspath(os.getcwd())
    plt.imsave("{}/{}/Tested_{}".format(path,dirName,image_name), image_result, cmap = plt.cm.gray )
    

"""
def TestFT_for_Specks(image_gray):
    p1 =  ndimage.filters.gaussian_filter((fourierTransform(image_gray, LPF(image_gray, 3500))), sigma = 4)
    gamma_corrected = exposure.adjust_gamma(abs(p1), 1.3) 
    sobel = filters.sobel(gamma_corrected)    
    sobel = exposure.adjust_gamma(sobel, 0.80)
    maxime = ndimage.maximum_filter(sobel ,  size=9,  mode='nearest')
    p3 = exposure.adjust_gamma(maxime, 0.95)
    th = filters.threshold_yen(p3)
    p4 = p3>th    
    remove_small_objects = morphology.remove_small_objects(p4, min_size=225)
    return(remove_small_objects)
"""

#%%

"""
# =============================================================================
# Clean Test
# =============================================================================
"""

#Path to your images for testing
images_path = "C:/Users/Deni/Desktop/workOrqa/zadatak3/Specks test/Clean/"
dirName = "Clean_test"


CreateFolder(dirName)
file_report = open(os.path.join(os.path.abspath(os.getcwd()),"Report_Clean.txt"),"w+")
for index, file in enumerate(os.listdir(images_path)):
    if file.endswith('.png'):
        image_name = file
        image = io.imread(images_path + image_name)
        image_gray = color.rgb2gray(image)
        image_result = FindSpecks(image_gray)
        ImageReport()
        
        report = WriteReport(image_result)
        file_report.write("Image: {}\n".format(index+1))
        file_report.write(report)
        print("{} finished".format(image_name))
file_report.close()
#%%
    
"""
# =============================================================================
# Dirty Test
# =============================================================================
"""
#Path to your images for testing
images_path = "C:/Users/Deni/Desktop/workOrqa/zadatak3/Specks test/Dirty/"
dirName = "Dirty_test"


CreateFolder(dirName)
file_report = open(os.path.join(os.path.abspath(os.getcwd()),"Report_Dirty.txt"),"w+")
for index, file in enumerate(os.listdir(images_path)):
    if file.endswith('.png'):
        image_name = file
        image = io.imread(images_path + image_name)
        image_gray = color.rgb2gray(image)
        image_result = FindSpecks(image_gray)
        ImageReport()
        
        report = WriteReport(image_result)
        file_report.write("Image: {}\n".format(index+1))
        file_report.write(report)
        print("{} finished".format(image_name))
file_report.close()





       

