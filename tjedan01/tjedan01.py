"""
    *[1] Osposobiti python, 
    *[1] Ucitavanje, prikaz, spremanje slika 

"""

from skimage import io
import numpy as np
import matplotlib.pyplot as plt
import skimage
import skimage.color
from skimage.color import convert_colorspace
from skimage import data

#Ucitavanje slike
first_image = io.imread("first_image.jpg")

#Slika je numpy array
print(first_image)

#Prikaz slike u pythonu
plt.imshow(first_image)
plt.title("Orginalna slika")
plt.show()


#Modificirajmo sliku, uzmimo jedan dio slikem te spremimo je
first_image_slice = first_image[500:600,500:600]
plt.imshow(first_image_slice)
plt.title("Slice slika")
#Naredba za spremanje slike
plt.savefig("sliceImageSave")
plt.show()


"""
    Manipulacija slike s odredenim bojama
"""

#Definirajmo boje
yellow_multiplier = [1, 1, 0]
red_multiplier = [1, 0, 0]


#Orginalnu sliku kada pomjesamo s zutom
plt.imshow(first_image*yellow_multiplier)
plt.title("Slika kada je pomnozimo s zutom bojom")
plt.show()

#Orginalnu sliku kada pomjesamo s crvenom
plt.imshow(first_image*red_multiplier)
plt.title("Slika kada je pomnozimo s crvenom bojom")
plt.show()

#%%
"""
    *[0]Modeli boja za prikaz slike (RGB,HSV,YIQ)
        -Što koji kanal predstavlja? Za što je pojedini model koristan, a što je
        potencijalno problematično? Kako se slike prevode iz jednog modela u
        drugi?
    *[1] Isprobati pretvorbe na rasponu slika, preko kanala manipulirati slikama
    (mijenjati boje, intenzitet i sl.)

"""

#Slika u RGB
blind_picture = io.imread("colorBlind.jpg")
plt.imshow(blind_picture)
plt.title("RGB")
plt.show()

#Pretvorba iz RGB->HSV
rgb_hsv = skimage.color.rgb2hsv(blind_picture)
plt.imshow(rgb_hsv)
plt.title("RGB -> HSV")
plt.show()


#Pretvorba iz RGB->YIQ
rgb_yiq = skimage.color.rgb2yiq(blind_picture)
plt.imshow(rgb_yiq)
plt.title(" RGB->YIQ")
plt.show()


#Pretvorba iz RGB->XYZ
rgb_xyz = skimage.color.rgb2xyz(blind_picture)
plt.imshow(rgb_xyz)
plt.title("RGB->XYZ")
plt.show()

#Pretvorba RGB-GRAY
rgb_gray = skimage.color.rgb2gray(blind_picture)
plt.imshow(rgb_gray)
plt.title(" RGB->GRAY")
plt.show()


img_astronaut = data.astronaut()
img_astronaut_hsv = skimage.color.convert_colorspace(img_astronaut, 'RGB', 'HSV')
plt.imshow(img_astronaut_hsv)
plt.title("RGB->HSV, colorspace")
plt.show()

img_astronaut_rgb = skimage.color.convert_colorspace(img_astronaut_hsv, 'HSV','RGB')
plt.imshow(img_astronaut_rgb)
plt.title("HSV->RGB, colorspace")
plt.show()


#%%
"""
    *[1] Greyscale
"""
from PIL import Image

#Drugi nacin ucitati sliku
#plt.imshow(first_image, "binary_r")
my_image = Image.open("first_image.jpg")

print(my_image.format)
#pretvorba slike u numpy array
my_imageNP = np.asarray(my_image)
plt.imshow(my_imageNP)
plt.show()


print(my_imageNP.shape)



my_imageNP2Gray = skimage.color.rgb2grey(my_imageNP)
print(my_imageNP2Gray.shape)
plt.imshow(my_imageNP2Gray)
plt.show()
plt.imshow(my_imageNP2Gray, 'gray') 

#%%
"""
    ✤ Za ilustraciju: usporedba histograma greyscale slike s histogramima R/G/B
        kanala te iste slike
     proučiti takav rezultat za nekoliko različitih slika, razumjeti kako oblik
        histograma odražava karakteristike slike - što se može očitati iz histograma,
        a što ne? Gdje su takvi prikazi korisni?
"""

from skimage import io


image_RGB = data.astronaut()


plt.imshow(image_RGB)
plt.title('Normal')
plt.colorbar()
plt.show()

#Posto je RGB ima tri kanal R,G,B


"Prikazimo histograme za R,G,B channels"""

print("\nPrikat R,G,B channels")
fig, (r_channel,g_channel,b_channel) = plt.subplots(ncols=3,figsize=(8, 4),sharex=True, sharey=True)
r_channel.imshow(image_RGB[:,:,0])
g_channel.imshow(image_RGB[:,:,1])
b_channel.imshow(image_RGB[:,:,2])
plt.show()

fig, (r_channel,g_channel,b_channel) = plt.subplots(ncols=3,figsize=(8, 4),sharex=True, sharey=True)
r_channel.imshow(image_RGB[:,:,0], plt.get_cmap('gray'))
g_channel.imshow(image_RGB[:,:,1], plt.get_cmap('gray'))
b_channel.imshow(image_RGB[:,:,2], plt.get_cmap('gray'))
plt.show()



#Pretvorba RGB slike to gray


"""Histogram"""
#Za svaki R/G/B prikaz
plt.title("R/G/B prikaz")
plt.hist(image_RGB[:,:,0].ravel(), bins=30, color='red', alpha=0.5 )
plt.hist(image_RGB[:,:,1].ravel(), bins=30, color='green', alpha=0.5 )
plt.hist(image_RGB[:,:,2].ravel(), bins=30, color='blue', alpha=0.5 )
plt.show()

""" Gray image"""
image_GRAY =  skimage.color.rgb2gray(image_RGB)



"""Usporedba Gray s R-kanalom"""

fig, (r_channel,gray_channel) = plt.subplots(ncols=2,figsize=(8, 4),sharex=True, sharey=True)
r_channel.imshow(image_RGB[:,:,0])
gray_channel.imshow(image_GRAY)
fig.suptitle("Prikaz R-kanala i Gray slike")
plt.show()



fig, (r_channel,gray_channel) = plt.subplots(ncols=2,figsize=(8, 4))
r_channel.hist(image_RGB[:,:,0].ravel(), bins=30, color='red', alpha=0.7 )
gray_channel.hist(image_GRAY.ravel(), bins=30, color='gray', alpha=0.7 )
fig.suptitle("Prikaz R-kanala i Gray  histograma")
plt.show()


"""Usporedba Gray s G-kanalom"""

fig, (r_channel,gray_channel) = plt.subplots(ncols=2,figsize=(8, 4),sharex=True, sharey=True)
r_channel.imshow(image_RGB[:,:,1])
gray_channel.imshow(image_GRAY)
fig.suptitle("Prikaz G-kanala i Gray slike")
plt.show()

fig, (r_channel,gray_channel) = plt.subplots(ncols=2,figsize=(8, 4))
r_channel.hist(image_RGB[:,:,1].ravel(), bins=30, color='green', alpha=0.7 )
gray_channel.hist(image_GRAY.ravel(), bins=30, color='gray', alpha=0.7 )
fig.suptitle("Prikaz G-kanala i Gray  histograma")
plt.show()


"""Usporedba Gray s G-kanalom"""

fig, (r_channel,gray_channel) = plt.subplots(ncols=2,figsize=(8, 4),sharex=True, sharey=True)
r_channel.imshow(image_RGB[:,:,1])
gray_channel.imshow(image_GRAY)
fig.suptitle("Prikaz G-kanala i Gray slike")
plt.show()

fig, (r_channel,gray_channel) = plt.subplots(ncols=2,figsize=(8, 4))
r_channel.hist(image_RGB[:,:,1].ravel(), bins=30, color='blue', alpha=0.7 )
gray_channel.hist(image_GRAY.ravel(), bins=30, color='gray', alpha=0.7 )
fig.suptitle("Prikaz G-kanala i Gray  histograma")
plt.show()
#%%

"""
✤ Formati slike (float, uint, int, ubyte, bool)
     Što predstavljaju i na što se treba paziti prilikom pretvaranja iz jednog u
    drugi?
     Usporediti float/float32/float64
     Usporediti uint/int
     Isprobati pretvorbe iz ubyte u ostale
     Simulirati oduzimanje ili zbrajanje dvije slike (ili sl. manipulacije)

"""
"""
Data type  Range

uint8     0 to 255
uint16    0 to 65535
uint32    0 to 2^32 - 1
float     -1 to 1 or 0 to 1
int8      -128 to 127
int16     -32768 to 32767
int32     -2^31 to 2^31 - 1

"""



from skimage.util import img_as_float
from skimage.util import img_as_ubyte
from skimage.util import img_as_uint

my_image = io.imread("first_image.jpg")


#conver numpy array to float
#my_imageFloat = img_as_float(my_image)
#my_imageUbyte = img_as_ubyte(my_image)
#my_imageUnit = img_as_uint(my_image)

float_image = np.float64(my_image)
int_image = np.uint8(float_image)
ubyte_image = np.ubyte(my_image)

ubyte_float = np.float32(ubyte_image)
print(ubyte_float)

plt.imshow(ubyte_float)


#%%
from skimage.transform import resize

astronaut_face = img_astronaut[80:180,180:260]
plt.imshow(astronaut_face)
plt.axis('off')
plt.show()
print(astronaut_face.shape)


test = img_astronaut[412:,362:442]

img_astronaut[412:,362:442] = astronaut_face

plt.imshow(img_astronaut)



























