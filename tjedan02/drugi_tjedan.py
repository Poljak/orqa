from PIL import Image
import os
from skimage import io
import numpy as np
import matplotlib.pyplot as plt
import skimage
from skimage import data

from scipy import ndimage
from scipy.ndimage import distance_transform_edt
from skimage import morphology, segmentation
from skimage.morphology import disk


"""IMPORTANT"""
from skimage import filters
from skimage import color
from skimage import feature
from skimage import measure
#%%
"""
# =============================================================================
# || The Different Types of Image Segmentation ||
# =============================================================================

    - semantic segmentation
        -> every pixel belongs to a particular class (either background or person)
    - instance segmentation
        -> different objects of the same class have different colors
        

# =============================================================================
# || Region-based Segmentation ||
# =============================================================================


One simple way to segment different objects could be to use their pixel values.
An important point to note – the pixel values will be different for the objects
and the image’s background if there’s a sharp contrast between them.

In this case, we can set a threshold value.
The pixel values falling below or above
that threshold can be classified accordingly
(as an object or the background). 
This technique is known as Threshold Segmentation.


# =============================================================================
# || GLOBAL THRESHOLD ||
# =============================================================================

 
If we want to divide the image into two regions (object and background)
we define a single threshold value. 
This is known as the global threshold.


# =============================================================================
# || LOCAL THRESHOLD ||
# =============================================================================

    
If we have multiple objects along with the background
we must define multiple thresholds. 
These thresholds are collectively known as the local threshold.


# =============================================================================
# || Edge Detection Segmentation ||
# =============================================================================


What divides two objects in an image? There is always
an edge between two adjacent regions with different
grayscale values (pixel values). 
The edges can be considered as the discontinuous local features of an image.

# =============================================================================
# || Morphometrics ||
# =============================================================================

Morphometrics involves counting the number of objects in an image
analyzing the size of the objects, or analyzing the shape of the objects.
"""


#%%
"""
# =============================================================================
# || Edge Detection Segmentation ||
# =============================================================================


What divides two objects in an image? There is always
an edge between two adjacent regions with different
grayscale values (pixel values). 
The edges can be considered as the discontinuous local features of an image.

"""
#canny, sigma important 

coins = data.coins()

edges =skimage.feature.canny(coins, sigma = 3)

edge_3 = skimage.feature.canny(coins, sigma = 2)
edge_1 = skimage.feature.canny(coins, sigma = 1)

f, (ax0, ax1) = plt.subplots(1,2, figsize=(12,5))
ax0.imshow(edge_3, cmap= 'gray' ) 
ax1.imshow(edge_1, cmap= 'gray' ) 
plt.show()

#%%
"""
# =============================================================================
# How to loop over images in folder and print them
# =============================================================================
"""

for file in os.listdir('.'):
    if file.endswith('.png'):
        print(file)
#%%
"""
# =============================================================================
# Divide image_name and image_extension
# =============================================================================
"""
for file in os.listdir('.'):
    if file.endswith('.png'):
        my_image = Image.open(file)
        #example "image0312.jpg" we will split into name=image0312, ext=jpg
        file_name, file_extension = os.path.splitext(file)
        #print("Image name: {}".format(file_name))
        #print("{} extension: {}".format(file_name,file_extension))

#%%
"""
# =============================================================================
# Resize image and save it to another folder in your diretory
# =============================================================================

"""
image_size300 = (300,300)

image_size700 = (700,700)

for file in os.listdir('.'):
    if file.endswith('.png'):
        my_image = Image.open(file)
        #Split name and extenstion of image
        my_image_name, my_image_extension = os.path.splitext(file)
        #Resize image
        my_image.thumbnail(image_size700)
        #Save resized image
        my_image.save("resize_700/{}_700{}".format(my_image_name, my_image_extension))
#%%
"""
Example of using thresholding

# =============================================================================
# Step 1: Reading & displaying the image (Leukemic cells)
# =============================================================================
"""


leukemic_cells = io.imread("leukemic_cells.png")
leukemic_cells_gray = color.rgb2gray(leukemic_cells)


fig, axes = plt.subplots(ncols=2, figsize=(12, 5) )
ax = axes.ravel()

ax[0].imshow(leukemic_cells)
ax[0].set_title('RGB')
ax[1].imshow(leukemic_cells_gray, cmap = 'gray')
ax[1].set_title('GRAY')

#%%
"""
# =============================================================================
# Step 2: Thresholding
# =============================================================================
"""
#thresh = filters.threshold_otsu(leukemic_cells_gray, nbins = 3)

#best option
thresh = filters.threshold_yen(leukemic_cells_gray)

thresholded_LC = leukemic_cells_gray < thresh

fig, axes = plt.subplots(ncols=2, figsize=(12, 5) )
ax = axes.ravel()

ax[0].imshow(leukemic_cells_gray, cmap = 'gray')
ax[0].set_title('Gray')
ax[1].imshow(thresholded_LC, cmap = 'gray')
ax[1].set_title('Thresholded')
#%%
"""
# =============================================================================
# Step 3: Cleaning up the mask
# =============================================================================

Mask will have some noise.
Use morphology operators to clean up the mask.
"""
# min_size removing small objects
remove_small_objects = morphology.remove_small_objects(thresholded_LC, min_size=250)

thresholded_LC_removeSmall = morphology.binary_closing(remove_small_objects, disk(3))

fig, axes = plt.subplots(ncols=2, figsize=(12, 5) )
ax = axes.ravel()

ax[0].imshow(thresholded_LC, cmap = 'gray')
ax[0].set_title('Thresholded without removing small objects')
ax[1].imshow(thresholded_LC_removeSmall, cmap = 'gray')
ax[1].set_title('Thresholded with removed small objects')
plt.show()

#leukemic_cells[thresholded_LC_removeSmall == False] = 0
#plt.imshow(leukemic_cells, cmap='gray')
#plt.title('Masked orginal image')
#plt.show()


#%%
"""
# =============================================================================
# Step 5: Image analysis
# =============================================================================
Count how many of the same objects(leukemic cells) we have
"""

#print(thresh)
#print(thresholded_LC_removeSmall.shape)

drops = ndimage.binary_fill_holes(thresholded_LC_removeSmall > thresh)
plt.imshow(drops, cmap = "gray")
plt.show()

labels = measure.label(drops)
print("We have :", labels.max(), "leukemic cells")

#%%
"""
# =============================================================================
# Final results
# =============================================================================
"""

fig, axes = plt.subplots(ncols=4, figsize=(13, 13) )
ax = axes.ravel()

ax[0].imshow(leukemic_cells)
ax[0].set_title('Leukemic cells')

ax[1].imshow(leukemic_cells_gray, cmap = 'gray')
ax[1].set_title('Grayscale')

ax[2].imshow(thresholded_LC_removeSmall, cmap = 'gray')
ax[2].set_title('Thresholded')

leukemic_cells[thresholded_LC_removeSmall == False] = 0
ax[3].imshow(leukemic_cells, cmap='gray')
ax[3].set_title('Masked orginal image')
plt.show()

